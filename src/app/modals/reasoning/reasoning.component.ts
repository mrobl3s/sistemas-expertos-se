import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-reasoning',
  templateUrl: './reasoning.component.html',
  styleUrls: ['./reasoning.component.scss']
})
export class ReasoningComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: { accepted: string[], rejected: string[], animal: string }) { }

}
