import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subject } from 'rxjs';
import * as csv from 'papaparse';
import { AnimalInterface } from './models/animal.interface';
import { Asserts } from './constants/asserts';
import { MatDialog } from '@angular/material/dialog';
import { ReasoningComponent } from './modals/reasoning/reasoning.component';

type Item = 'consult' | 'see';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav | undefined;

  animals: AnimalInterface[] = [];
  activeItem: Item = 'consult';
  question: string | undefined;
  questionIndex = -1;
  animal: AnimalInterface | undefined;
  animalIndex = -1;
  rejected: string[] = [];
  accepted: string[] = [];
  result = '';
  showResult = false;
  displayedColumns: string[] = ['name', 'attributes', 'type'];

  // Sidenav
  sidenavMode: 'side' | 'over' = 'side';
  sidenavOpened = true;

  private onDestroy = new Subject<void>();

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    this.loadCSV();
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  isItemActive(item: Item): string {
    return item === this.activeItem ? 'active' : '';
  }

  setActiveItem(item: Item): void {
    this.activeItem = item;
    this.startConsult();
  }

  accept(): void {
    const attribute = this.animal?.attributes[this.questionIndex];
    if (attribute != null) {
      this.accepted.push(attribute);
    }
    if (!this.animal?.attributes[this.questionIndex + 1]) {
      this.found();
    } else {
      this.nextQuestion();
    }
  }

  reject(): void {
    const attribute = this.animal?.attributes[this.questionIndex];
    if (attribute != null) {
      this.rejected.push(attribute);
    }
    this.nextAnimal();
  }

  startConsult(): void {
    this.animalIndex = -1;
    this.questionIndex = -1;
    this.showResult = false;
    this.rejected = [];
    this.accepted = [];
    this.nextAnimal();
  }

  continue(): void {
    this.showResult = false;
    this.nextAnimal();
  }

  openModal(): void {
    this.dialog.open(ReasoningComponent, {
      height: '600px',
      width: '800px',
      data: {accepted: this.accepted, rejected: this.rejected, animal: this.animal?.name}
    });
  }

  private loadCSV(): void {
    csv.parse('../assets/zoo.csv', {
      complete: (results: csv.ParseResult<string[]>) => {
        this.parseCSV(results);
      },
      download: true,
      encoding: 'utf-8'
    });
  }

  private parseCSV(results: csv.ParseResult<string[]>): void {
    for (let i = 1; i <= 100; i++) {
      const name = results.data[i].shift();
      const type = results.data[i].pop();

      this.animals.push({
        name: name ? name : '',
        attributes: this.getAnimalAttributes(results.data[i]),
        type: type ? type : ''
      });
    }

    this.nextAnimal();
  }

  private getAnimalAttributes(attributes: string[]): string[] {
    return attributes
      .map((value, i) => Number(value) > 0 ? Asserts[i].replace(':x', value) : '')
      .filter(value => value !== '');
  }

  private nextAnimal(): void {
    this.animalIndex++;
    this.animal = this.animals[this.animalIndex];
    if (this.animal != null) {
      if (!this.isRejected() && !this.hasNotRequired()) {
        this.questionIndex = -1;
        this.nextQuestion();
      } else {
        this.nextAnimal();
      }
    } else {
      this.notFound();
    }
  }

  private nextQuestion(): void {
    this.questionIndex++;
    this.question = this.animal?.attributes[this.questionIndex];
    if (this.question != null) {
      if (!this.isAsked()) {
        this.question = `¿${this.question}?`;
      } else {
        this.nextQuestion();
      }
    } else {
      this.nextAnimal();
    }
  }

  private notFound(): void {
    this.result = 'No encontré el animal que quieres ingresar';
    this.showResult = true;
  }

  private isRejected(): boolean {
    for (const rejectedAttribute of this.rejected) {
      if (this.animal?.attributes != null) {
        for (const attribute of this.animal.attributes) {
          if (rejectedAttribute === attribute) {
            return true;
          }
        }
      }
    }

    return false;
  }

  private hasNotRequired(): boolean {
    let reject = false;

    for (const acceptedAttribute of this.accepted) {
      reject = true;

      if (this.animal?.attributes != null) {
        for (const attribute of this.animal.attributes) {
          if (acceptedAttribute === attribute) {
            reject = false;
            break;
          }
        }

        if (reject) {
          return reject;
        }
      }
    }

    return reject;
  }

  private isAsked(): boolean {
    for (const askedAttribute of this.accepted) {
      if (this.animal?.attributes) {
        if (this.animal.attributes[this.questionIndex] === askedAttribute) {
          return true;
        }
      }
    }

    return false;
  }

  private found(): void {
    this.result = `El animal es ${this.animal?.name}, puedes clasificarlo como ${this.animal?.type}`;
    this.showResult = true;
  }
}
