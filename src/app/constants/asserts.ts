export const Asserts = [
  'Tiene pelo',
  'Tiene plumas',
  'Pone huevos',
  'Produce leche',
  'Es volador',
  'Es acuático',
  'Es depredador',
  'Tiene dientes',
  'Tiene columna vertebral',
  'Respira oxígeno',
  'Es venenoso',
  'Tiene aletas',
  'Tiene :x patas',
  'Tiene cola',
  'Es doméstico'
];
