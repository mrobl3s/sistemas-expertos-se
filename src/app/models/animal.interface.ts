export interface AnimalInterface {
  name: string;
  attributes: string[];
  type: string;
}
